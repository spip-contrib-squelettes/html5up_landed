<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Formulaire de configuration de htlm5_landed,
 * déclaration des saisies.
 * @return array
 **/
function formulaires_configurer_html5_landed_saisies_dist(): array {
	// $saisies est un tableau décrivant les saisies à afficher dans le formulaire de configuration
	$saisies = [
	'options' => [
		// Changer l'intitulé du bouton final de validation
	'texte_submit' => '<:bouton_enregistrer:>'
				],
		[	// partie sommaire
		'saisie' => 'fieldset',
		'options' => [
			'nom' => 'accueil',
			'label' => '<:html5_landed:article_banner:>'
					],
		'saisies' =>	[
				[		// article sommaire
			'saisie' => 'selecteur_article',
			'options' => [
			'nom' => 'article_banner',
			'label' => '<:html5_landed:article:>'
						]     
				],
				[		// image sommaire
			'saisie' => 'selecteur_document',
			'options' => [
			'nom' => 'image_banner',
			'label' => '<:html5_landed:image_banner:>'
						],
			'verifier' => [
				'type' => 'entier',
				'options' => [
					'min' => 1
							]
						]
				],
				[		// explication opacité
			'saisie' => 'explication',
			'options' => [
			'nom' => 'banner_opacite',
			'texte' => '<:html5_landed:banner_opacite:>'
						]
				],
				[		// opacité fond en haut
			'saisie' => 'input',
			'options' => [
			'nom' => 'banner_opacite1',
			'label' => '<:html5_landed:banner_opacite1:>',
			'defaut' => '0.95'
						],
			'verifier' => [
				'type' => 'decimal',
				'options' => [
					'min' => 0,
					'max' => 1,
					'nb_decimales' => 2,
					'separateur' => 'oui',
					'normaliser' => 'oui'
							]
						]
				],
				[		// opacité fond en bas
			'saisie' => 'input',
			'options' => [
			'nom' => 'banner_opacite2',
			'label' => '<:html5_landed:banner_opacite2:>',
			'defaut' => '0.95'
						],
			'verifier' => [
				'type' => 'decimal',
				'options' => [
					'min' => 0,
					'max' => 1,
					'nb_decimales' => 2,
					'separateur' => 'oui',
					'normaliser' => 'oui'
							]
						]
				]
			]   // fin saisies accueil
		],	// fin champ accueil
		[	// partie theme
		'saisie' => 'fieldset',
		'options' => [
			'nom' => 'theme',
			'label' => '<:html5_landed:configuration_theme:>'
					],
		'saisies' =>	[
				[		// couleur bandeau
			'saisie' => 'couleur',
			'options' => [
			'nom' => 'couleur_bando',
			'label' => '<:html5_landed:couleur_bando:>',
			'defaut' => '#272727',
			'maxlength' => '7'
						],
			'verifier' => [
				'type' => 'couleur',
				'options' => [
					'type' => 'hexa',
					'normaliser' => 'oui'
							]
						]
				],
				[		// couleur fond rub active ds menu
			'saisie' => 'couleur',
			'options' => [
			'nom' => 'couleur_actif',
			'label' => '<:html5_landed:couleur_actif:>',
			'defaut' => '#e44c65',
			'maxlength' => '7'
						],
			'verifier' => [
				'type' => 'couleur',
				'options' => [
					'type' => 'hexa',
					'normaliser' => 'oui'
							]
						]
				],
				[		// couleur bordutres formulaires
			'saisie' => 'couleur',
			'options' => [
			'nom' => 'couleur_theme',
			'label' => '<:html5_landed:couleur_theme:>',
			'defaut' => '#e44c65',
			'maxlength' => '7'
						],
			'verifier' => [
				'type' => 'couleur',
				'options' => [
					'type' => 'hexa',
					'normaliser' => 'oui'
							]
						]
				],
				[		// couleur liens
			'saisie' => 'couleur',
			'options' => [
			'nom' => 'couleur_actif',
			'label' => '<:html5_landed:couleur_liens:>',
			'defaut' => '#e44c65',
			'maxlength' => '7'
						],
			'verifier' => [
				'type' => 'couleur',
				'options' => [
					'type' => 'hexa',
					'normaliser' => 'oui'
							]
						]
				],
				[		// fil d'ariane oui/non
			'saisie' => 'case',
			'options' => [
			'nom' => 'fil_ariane',
			'label' => '<:html5_landed:fil_ariane:>',
			'explication' => '<:html5_landed:fil_ariane_explication:>',
			'defaut' => 'non'
						]
				],
				[		// 1er doc en logo par default oui/non
			'saisie' => 'case',
			'options' => [
			'nom' => 'logo_document',
			'label' => '<:html5_landed:logo_document:>',
			'explication' => '<:html5_landed:logo_document_explication:>',
			'defaut' => 'oui'
						]
				]
						]	// fin saisies theme
		]	// fin champ theme
	];
	return $saisies;
}
