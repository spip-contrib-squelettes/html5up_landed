# Changelog

## 1.3.3 - 2023-11-13

### Added
- ajout de formulaires/configurer_html5_landed.php (vidage de formulaires/configurer_html5_landed.html)

### Changed

- #2 déclarer les Saisies en PHP (formulaire de configuration) et utilisation palette pour le choix de couleurs
- #3 dépendance au plugin yaml

## 1.3.0 - 2023-06-23

### Added
- ajout de fichiers README.md et CHANGELOG.md

### Changed

- #1 compatibilité SPIP 4.0+
- pagination SPIP 4
